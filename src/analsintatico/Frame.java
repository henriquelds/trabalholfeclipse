/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analsintatico;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

/**
 *
 * @author Henrique
 */
public class Frame extends javax.swing.JFrame {

    /**
     * Creates new form Frame
     */
    public Frame() {
        super("Analisador Léxico e Sintático do Leão");
        initComponents();
        TextLineNumber tln = new TextLineNumber(jTextPane1);
        jScrollPane1.setRowHeaderView(tln);
        ImageIcon windowIcon = new ImageIcon(getClass().getResource("/resources/img/windowIcon.png"));
        this.setIconImage(windowIcon.getImage());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        AnalisaButton = new javax.swing.JButton();
        imgLabel = new javax.swing.JLabel();
        ComboFonts = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        ComboSize = new javax.swing.JComboBox();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        AbrirMenu = new javax.swing.JMenuItem();
        SalvarMenu = new javax.swing.JMenuItem();
        SairMenu = new javax.swing.JMenuItem();
        SobreMenu = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextPane1.setFont(new java.awt.Font("Monospaced", 0, 13)); // NOI18N
        jScrollPane1.setViewportView(jTextPane1);

        AnalisaButton.setText("Analisa, Brasil!!!");
        AnalisaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnalisaButtonActionPerformed(evt);
            }
        });

        ComboFonts.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Monospace", "DejaVu Sans Mono", "Consolas", "Courier New" }));
        ComboFonts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboFontsActionPerformed(evt);
            }
        });

        jLabel1.setText("Fonte:");

        ComboSize.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "11", "13", "15", "17" }));
        ComboSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboSizeActionPerformed(evt);
            }
        });

        jMenu1.setText("Arquivo");

        AbrirMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        AbrirMenu.setText("Abrir");
        AbrirMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirMenuActionPerformed(evt);
            }
        });
        jMenu1.add(AbrirMenu);

        SalvarMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        SalvarMenu.setText("Salvar");
        SalvarMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalvarMenuActionPerformed(evt);
            }
        });
        jMenu1.add(SalvarMenu);

        SairMenu.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        SairMenu.setText("Sair");
        SairMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SairMenuActionPerformed(evt);
            }
        });
        jMenu1.add(SairMenu);

        jMenuBar1.add(jMenu1);

        SobreMenu.setText("Sobre");
        SobreMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                SobreMenuMousePressed(evt);
            }
        });
        jMenuBar1.add(SobreMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ComboFonts, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ComboSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AnalisaButton, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                    .addComponent(imgLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(ComboFonts, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ComboSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(AnalisaButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(imgLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 248, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 21, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AnalisaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnalisaButtonActionPerformed
        String str = jTextPane1.getText();
        try {
            AnalSintatico as = new AnalSintatico(str);
            if(as.I()){
            	ImageIcon ic = new ImageIcon(getClass().getResource("/resources/img/ok.jpg"));
                imgLabel.setIcon(ic);
                //String sucesso = "songs\\sucesso.wav";
                InputStream in = getClass().getResourceAsStream("/resources/songs/sucesso.wav"); //new FileInputStream(sucesso);
                AudioStream audioStream = new AudioStream(in);
                AudioPlayer.player.start(audioStream);
            }
            else{
                ImageIcon ic = new ImageIcon(getClass().getResource("/resources/img/erro.png"));
                imgLabel.setIcon(ic);
            }
        } catch (IOException ex) {
            Logger.getLogger(Frame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_AnalisaButtonActionPerformed

    private void SairMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SairMenuActionPerformed
        System.exit(1);
    }//GEN-LAST:event_SairMenuActionPerformed

    private void SalvarMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalvarMenuActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        LeaoFilter lfilter = new LeaoFilter();
        fileChooser.setFileFilter(lfilter);
        
        int r = fileChooser.showSaveDialog(this);
        if( r == JFileChooser.APPROVE_OPTION){
            File sFile = new File(fileChooser.getSelectedFile()+".leao");
            String file_name = sFile.getName();
            String file_path = sFile.getParent();

            try{
                if(!sFile.exists()) {
                    sFile.createNewFile();
                    BufferedWriter out = new BufferedWriter(new FileWriter(sFile));
                    out.write(jTextPane1.getText());
                    out.close();
                    JOptionPane.showMessageDialog(null, "Arquivo " + file_name + " criado com sucesso em \n" + file_path);    
                } 
                else {
                    String message = "Arquivo " + file_name + " já existe in \n" + file_path + ":\n" + "Deseja sobrescrever?";
                    String title = "Aviso";
                    int reply = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
                    if(reply == JOptionPane.YES_OPTION){
                        sFile.delete();
                        sFile.createNewFile();
                        BufferedWriter out = new BufferedWriter(new FileWriter(sFile));
                        out.write(jTextPane1.getText());
                        out.close();
                        JOptionPane.showMessageDialog(null, "Arquivo " + file_name + " sobrescrito com sucesso em \n" + file_path);
                    }
                }
            }
            catch(IOException e) {
                JOptionPane.showMessageDialog(null, "Arquivo não salvo", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_SalvarMenuActionPerformed

    private void AbrirMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirMenuActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        LeaoFilter lfilter = new LeaoFilter();
        fileChooser.setFileFilter(lfilter);
        
        int r = fileChooser.showOpenDialog(this);
        if( r == JFileChooser.APPROVE_OPTION){
            File leao = fileChooser.getSelectedFile();
            try {
                String content = new String(Files.readAllBytes(Paths.get(leao.getAbsolutePath())));
                jTextPane1.setText(content);
                
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(null, "Arquivo não encontrado", "Erro", JOptionPane.ERROR_MESSAGE);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Arquivo não encontrado", "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
        
    }//GEN-LAST:event_AbrirMenuActionPerformed

    private void SobreMenuMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SobreMenuMousePressed
    	//System.out.println(getClass().getResource("../resources/img/ufpel.png"));
    	ImageIcon icon = new ImageIcon(getClass().getResource("/resources/img/ufpel.png"));
        JOptionPane.showMessageDialog(null, "Trabalho desenvolvido para a disciplina de Linguagens Formais 2015/1\n Alunos: Felipe Lopes, Helena Dufau, Henrique Lemos\n Professora: Luciana Foss\n UFPel", "Sobre", JOptionPane.INFORMATION_MESSAGE, icon);
    	
    }//GEN-LAST:event_SobreMenuMousePressed

    private void ComboFontsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboFontsActionPerformed
        String name = ComboFonts.getSelectedItem().toString();
        int size = Integer.valueOf(ComboSize.getSelectedItem().toString());
        Font font = new Font(name, 0, size);
        jTextPane1.setFont(font);
    }//GEN-LAST:event_ComboFontsActionPerformed

    private void ComboSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboSizeActionPerformed
        String name = ComboFonts.getSelectedItem().toString();
        int size = Integer.valueOf(ComboSize.getSelectedItem().toString());
        Font font = new Font(name, 0, size);
        jTextPane1.setFont(font);
    }//GEN-LAST:event_ComboSizeActionPerformed

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem AbrirMenu;
    private javax.swing.JButton AnalisaButton;
    private javax.swing.JComboBox ComboFonts;
    private javax.swing.JComboBox ComboSize;
    private javax.swing.JMenuItem SairMenu;
    private javax.swing.JMenuItem SalvarMenu;
    private javax.swing.JMenu SobreMenu;
    private javax.swing.JLabel imgLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables
    public class LeaoFilter extends FileFilter{
    @Override
    public boolean accept(File f){
        return f.getName().toLowerCase().endsWith(".leao")||f.isDirectory();
    }
    @Override
    public String getDescription(){
        return "Leao files (*.leao)";
    }
}


}
