/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analsintatico;

import java.io.IOException;
import java.util.Stack;

/**
 *
 * @author Henrique
 */
public class AnalSintatico {

    /**
     * @param args the command line arguments
     */
    private AnalLexico al;
    private Stack<Token> tokens;
    
    public AnalSintatico(String str) throws IOException{
        al = new AnalLexico(str);
        tokens = new Stack<Token>();
    }
    
    public boolean I() throws IOException{
        Token tk = removeToken();
        if(Lista_sta(tk)){
            tk = removeToken();
            if(tk == Token.EOF){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            
            return false;
        }
    }
    
    private boolean Lista_sta(Token tk) throws IOException{
        
        if(Sta(tk)){
           tk = removeToken();
           if(Lista_sta(tk)){
               return true;
           }
           else{
               return false;
           }
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    private boolean Sta(Token tk) throws IOException{
        if(tk == Token.VARIAVEL){
            tk = removeToken();
            if(tk == Token.ATRIBUICAO){
                tk = removeToken();
                if(Exp2(tk)){
                    tk = removeToken();
                    if(tk == Token.DELIMITADOR_PV){
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private boolean Exp2(Token tk) throws IOException{
        if(Exp4(tk)){
            tk = removeToken();
            if(Exp2linha(tk)){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    
    private boolean Exp2linha(Token tk) throws IOException{
        if(tk == Token.CONJUNCAO){
            tk = removeToken();
            if(Exp4(tk)){
                tk = removeToken();
                if(Exp2linha(tk)){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            tokens.push(tk);
            return true;
        }
    }
    
    private boolean Exp4(Token tk){
        if(tk == Token.VALOR_LOG || tk == Token.VARIAVEL){
            return true;
        }
        else {
            return false;
        }
    }
    
    private Token removeToken() throws IOException{
        if(!tokens.empty()){
            return tokens.pop();
        }
        else{
            return al.le_token();
        }
    }
    
    public void printToken(Token t){
        switch(t){
            case VARIAVEL : System.out.println("Variável");break;
            case DELIMITADOR_V : System.out.println("Delimitador Vírgula");break;
            case DELIMITADOR_PV : System.out.println("Delimitador Ponto-e-vírgula");break;    
            case IF : System.out.println("If");break;
            case ELSE : System.out.println("Else");break;
            case PRINT : System.out.println("Print");break;
            case READ : System.out.println("Read");break;
            case VALOR_LOG : System.out.println("Valor lógico");break;
            case EOF : System.out.println("EOF");break;
            case ERRO : System.out.println("Erro");break;
            case APAR : System.out.println("Abre parêntese");break;  
            case FPAR : System.out.println("Fecha parêntese");break; 
            case ACHAVE : System.out.println("Abre chave");break;
            case FCHAVE : System.out.println("Fecha chave");break;
            case NEGACAO : System.out.println("Negação"); break;
            case DISJUNCAO : System.out.println("Disjunção"); break;
            case CONJUNCAO: System.out.println("Conjunção"); break;
            case IMPLICACAO: System.out.println("Implicação"); break;
            case EQUIVALENCIA: System.out.println("Equivalência"); break;    
            case ATRIBUICAO: System.out.println("Atribuição"); break;    
            default :
        }
    }
    
}
