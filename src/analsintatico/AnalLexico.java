/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package analsintatico;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Henrique
 */
public class AnalLexico {
    
    private int currentRow;
    private int currentColumn;
    private PushbackReader pr;                              //EOF (bug), EOF, TAB, \N, \R, ',  (,   ), , , ;, { ,  }, ^, =, -, <, >
    private static ArrayList<Integer> J = new ArrayList<Integer>(Arrays.asList(65535,-1, 9, 10, 13, 32, 39, 40, 41, 44,59, 123, 125, 94, 61, 45, 60, 62));  
    
    public AnalLexico(String str) throws FileNotFoundException, IOException{
        pr = new PushbackReader(new StringReader(str));
        currentRow = 1;
        currentColumn = 1;
        
    }
    
    public Token le_token() throws IOException{
        Token t = Token.ERRO;
        Estado currentState = Estado.E0;
        boolean tokenFound = false;
        //String identificador="";
        StringBuilder identificador = new StringBuilder();
        
        while(!tokenFound){
            int ch = pr.read();
            //System.out.printf("%d\n", ch);
            //--------------------E0----------------------
                if(currentState == Estado.E0){
                    if(belongsToK(ch)){ // pertence a K, qualquer letra maius exceto V ou F, underline e qualquer letra minus exceto v
                        currentState = Estado.var0; 
                        currentColumn++; 
                        identificador.delete(0, identificador.length());
                        identificador.append(Character.toChars(ch));
                    }
                    else if(ch == 118){      // v
                        currentState = Estado.dis0; 
                        currentColumn++; 
                        identificador.delete(0, identificador.length());     //pode se tornar variavel mais adiante, reseta o sb
                        identificador.append(Character.toChars(ch));        //e adiciona o char vá que ele vire variavel ou palav reserv mais adiante
                    }
                    else if(ch == 70 || ch == 86){  // V ou  F
                        currentState = Estado.val_log_let;
                        currentColumn++;
                        identificador.delete(0, identificador.length());     //pode se tornar variavel mais adiante, reseta o sb
                        identificador.append(Character.toChars(ch));         //e adiciona o char vá que ele vire variavel ou palav reserv mais adiante
                    }
                    else if(ch == 48 || ch == 49){ // 0 ou  1
                        currentState = Estado.val_log_num;
                        currentColumn++;   //nao pode ser variavel nem palav reserv, nao precisa armazenar char no stringbuilder
                    }
                    else if( ch == 94){   // ^
                        currentState = Estado.con0;
                        currentColumn++;
                    }
                    else if( ch == 45){  // -
                        currentState = Estado.imp0;
                        currentColumn++;
                    }
                    else if( ch == 60){  // <
                        currentState = Estado.eq0;
                        currentColumn++;
                    }
                    else if( ch == 61){  // =
                        currentState = Estado.atrib0;
                        currentColumn++;
                    }
                    else if(J.contains(ch)){     //pula os espaços antes de achar qualquer token ou depois de achar um token valido
                        if(ch == 10){       //se for nova linha, zerao contador da coluna e incrementa o da linha
                            currentColumn = 0;
                            currentRow++;
                        }
                        else{
                            if(ch == 59 ){   //ponto e virg
                                t = Token.DELIMITADOR_PV;
                                return t;
                            }
                            else if( ch == 44){  // virgula
                                t = Token.DELIMITADOR_V;
                                return t;
                            }
                            else if( ch == 39){   // '
                                t = Token.NEGACAO;
                                return t;
                            }
                            else if( ch == 40){        // (
                                t = Token.APAR;
                                return t;
                            }
                            else if( ch == 41){   // )
                                t = Token.FPAR;
                                return t;
                            }
                            else if( ch == 123){   // {
                                t = Token.ACHAVE;
                                return t;
                            }
                            else if( ch == 125){   //{
                                t = Token.FCHAVE;
                                return t;
                            }
                            else if( ch == -1){   //EOF normal
                                t = Token.EOF;
                                return t;
                            }
                            else if( ch == 65535){   //EOF bugado do pushbackreader
                                t = Token.EOF;
                                return t;
                            }
                            else if( ch == 9){  //tab
                                currentColumn =  currentColumn + 3;     //aumenta 4 o contador de coluna qndo é tab
                            }
                            currentColumn++;       //se for \r só incrementa a coluna, nao sao tokens
                        }
                        currentState = Estado.E0;
                    }
                    else{
                        t = Token.ERRO; 
                        currentColumn++; 
                        //System.out.println(identificador.toString());
                        return t;
                    }
                    
                    // tem mais coisa no estado inicial.......
                } 

                //-----------------------------var0-----------------------
                else if(currentState == Estado.var0){
                    if(belongsToL(ch)){ // pertence a L, qualquer letra maius ou minus, qualquer digito e underline
                        currentState = Estado.var0; 
                        currentColumn++; 
                        identificador.append(Character.toChars(ch));
                    }
                    else if( J.contains(ch)){     //achou um delimitador fim pra variavel/palav reservada
                        pr.unread(ch);
                        //System.out.println("id "+identificador.toString());
                        t = testaIdentificador(identificador.toString());   //testa a concatenação de simbolos q obteve
                        return t;                                           //pra saber se é variavel ou palav reservada
                    }
                    else{
                        t = Token.ERRO; 
                        currentColumn++;
                        //System.out.println(Character.getNumericValue(ch));
                        return t;
                    }
                }       

                //--------------------dis0-----------------------
                else if(currentState == Estado.dis0){
                        if(J.contains(ch)){           // é o operador logico disjunção
                            pr.unread(ch); 
                            t = Token.DISJUNCAO;
                            return t;
                        }
                        else if(belongsToL(ch)){ // pertence a L, qualquer letra maius ou minus, qualquer digito e underline)
                            currentState = Estado.var0;       //nao é disjunçao, é variavel ou palav reserv
                            currentColumn++;
                            identificador.append(Character.toChars(ch));
                        }
                        else{
                            t = Token.ERRO; 
                            currentColumn++; 
                            return t;
                        }
                }        

                //---------------------val_log_let--------------------------
                else if(currentState == Estado.val_log_let){
                        if(J.contains(ch)){           // é algum valor logico expresso por V ou F
                            pr.unread(ch); 
                            t = Token.VALOR_LOG;
                            return t;
                        }
                        else if(belongsToL(ch)){ // pertence a L, qualquer letra maius ou minus, qualquer digito e underline)
                            currentState = Estado.var0;       //deve ser variavel ou palav reserv
                            currentColumn++;
                            identificador.append(Character.toChars(ch));
                        }
                        else{
                            t = Token.ERRO; 
                            currentColumn++; 
                            return t;
                        }
                }    

                //---------------val_log_num----------------------
                else if(currentState == Estado.val_log_num){
                        if(J.contains(ch)){           // achou delimitador, é algum valor logico expresso por 0 ou 1,
                            pr.unread(ch); 
                            t = Token.VALOR_LOG;
                            return t;
                        }
                        else{ //nao pode ter letra dps, se tiver é erro token nao identificado
                            t = Token.ERRO; currentColumn++; return t;
                        }
                }      
                
                //----------------con0-----------------
                else if(currentState == Estado.con0){
                    if(J.contains(ch) || belongsToL(ch)){     //se for 
                        pr.unread(ch);
                        t = Token.CONJUNCAO;
                        return t;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------imp0-----------------
                else if(currentState == Estado.imp0){
                    if(ch == 62){     //se for >
                        currentState = Estado.imp1;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------imp1-----------------
                else if(currentState == Estado.imp1){
                    if(J.contains(ch) || belongsToL(ch)){     //se for qualquer coisa do alfabeto
                        pr.unread(ch);
                        t = Token.IMPLICACAO;
                        return t;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------eq0-----------------
                else if(currentState == Estado.eq0){
                    if(ch == 45){     //se for -
                        currentState = Estado.eq1;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------eq1-----------------
                else if(currentState == Estado.eq1){
                    if(ch == 62){     //se for >
                        currentState = Estado.eq2;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------eq2-----------------
                else if(currentState == Estado.eq2){
                    if(J.contains(ch) || belongsToL(ch)){     //se for qualquer coisa do alfabeto
                        pr.unread(ch);
                        t = Token.EQUIVALENCIA;
                        return t;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------atrib0-----------------
                else if(currentState == Estado.atrib0){
                    if(ch == 58){     //se for :
                        currentState = Estado.atrib1;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------atrib1-----------------
                else if(currentState == Estado.atrib1){
                    if(ch == 61){     //se for =
                        currentState = Estado.atrib2;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }
                
                //----------------atrib2-----------------
                else if(currentState == Estado.atrib2){
                    if(J.contains(ch) || belongsToL(ch)){     //se for qualquer coisa do alfabeto
                        pr.unread(ch);
                        t = Token.ATRIBUICAO;
                        return t;
                    }
                    else{
                        t = Token.ERRO; currentColumn++; return t;
                    }
                }

        }
        return t;
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public int getCurrentColumn() {
        return currentColumn;
    }
    
    public boolean belongsToL(int ch){
        // qualquer letra maiuscula ou minuscula ou qualquer digito
        if((ch >= 48 && ch <= 57) || (ch >= 65 && ch <= 90) || (ch == 95) || (ch >= 97 && ch <= 122)){
            return true;
        }
        return false;
    }
    
    public boolean belongsToK(int ch){
        // qualquer letra maiuscula ou minuscula ou qualquer digito
        if((ch >= 65 && ch <= 69) || (ch >= 71 && ch <= 85) || (ch >= 87 && ch <= 90) || (ch == 95) || (ch >= 97 && ch <= 117) || (ch >= 119 && ch <= 122)){
            return true;
        }
        return false;
    }
    
    public Token testaIdentificador(String id){
        ArrayList reservadas = new ArrayList(Arrays.asList("if", "else", "Print", "Read", "true", "false"));
        Token t;
        
        if(reservadas.contains(id)){
            switch(id){
                case "if" : t = Token.IF; break;
                case "else" : t = Token.ELSE; break;
                case "Print" : t = Token.PRINT; break;
                case "Read" : t = Token.READ; break;
                case "true" : t = Token.VALOR_LOG; break;
                case "false" : t = Token.VALOR_LOG; break;
                default : t = Token.ERRO; 
            }
        }
        else{
            t = Token.VARIAVEL;
        }
        return t;
    }
    
    
}
